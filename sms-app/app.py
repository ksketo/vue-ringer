from __future__ import print_function
import time, requests, dateutil.parser, os, sys, ast
from pymongo import MongoClient
from twilio.rest import Client
from rfc3339 import rfc3339
from os.path import join, dirname
from dotenv import load_dotenv
from redis import Redis


dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

DATABASE = os.environ.get("DATABASE")
MLBAM_URL = os.environ.get("MLBAM_URL")
RPAYLOAD = os.environ.get("RPAYLOAD")
TWILIO_SID = os.environ.get("TWILIO_SID")
TWILIO_TOKEN = os.environ.get("TWILIO_TOKEN")
FROM_PHONE = os.environ.get("FROM_PHONE")


def main():
    # 1. Fetch phone data from mongo database
    phones = get_phones()
    print(phones, file=sys.stderr)

    r = Redis(host='redis', port=6379, db=0)
    client = r.pubsub()
    client.subscribe(['data'])
    for item in client.listen():
        print(item, file=sys.stderr)
        try:
            i = ast.literal_eval(item['data'])
            if type(i) is not dict:
                continue
        except:
            continue

        print('processing item', file=sys.stderr)
        print(i, file=sys.stderr)

        # 2. Fetch movie data from MLBAM for specific account
        shows = get_upcoming_shows()
        print(shows, file=sys.stderr)

        # 3. Send an sms to all numbers in db
        # for phone in phones:
        #     send_sms_to_phone(phone, shows)
        send_sms_to_phone(i['phoneNumber'], shows)

def get_phones():
    client = MongoClient(DATABASE)
    db = client['ringer-db']
    coll = db['users']
    cursor = coll.find()
    phones = [str(i['phoneNumber']) for i in cursor]
    return phones

def get_upcoming_shows():
    url = MLBAM_URL

    t = time.time()
    sd = rfc3339(t, utc=True)
    querystring = {"size":"15","offset":"0","start":sd,"end":"2017-12-15T00:00:00.000Z","expand":"1","filter_by":"airing_date","sort":"asc"}

    headers = {
        'Accept': "application/json",
        'Content-Type': "application/json",
        'Profile-ID': "265591",
        'reqPayload': RPAYLOAD,
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.ok)
    mlbam_data = []
    titles = set([])
    for item in response.json()['body']['items']:
        title = item['title']
        airing_date = item['airing_date']
        if (not title in titles) and airing_date and valid_date(airing_date):
            titles.add(item['title'])
            mlbam_data.append({
                'title': title,
                'airing_date': get_show_time(airing_date)
            })
    return mlbam_data

def valid_date(airing_date):
    print(airing_date)
    d = dateutil.parser.parse(airing_date)
    n = d.now()
    if d.day == n.day and (d.hour > n.hour or (d.hour == n.hour and d.minute > n.minute)):
        return True
    return False

def get_show_time(airing_date):
    d = dateutil.parser.parse(airing_date)
    h = d.hour
    m = d.minute
    if m < 10:
        m = '0{}'.format(m)
    return '{}.{}'.format(h, m)

def send_sms_to_phone(phone, shows):
    body = "PS VUE \n\nDon't forget your fav shows today! \n\n"
    for show in shows:
        body += '- {} | {} \n'.format(show['airing_date'], show['title'])

    account_sid = TWILIO_SID
    auth_token = TWILIO_TOKEN

    client = Client(account_sid, auth_token)

    client.api.account.messages.create(
        to="+44" + phone,
        from_=FROM_PHONE,
        body=body)


if __name__ == '__main__':
    main()
