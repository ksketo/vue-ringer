const express = require('express')
const router = express.Router()
const { catchErrors } = require('../handlers/errorHandlers')
const userController = require('../controllers/userController')


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Vue-Ringer' })
})

/* API */
router.get('/users', catchErrors(userController.getUserDetails))
router.post('/add/', catchErrors(userController.addUser))

module.exports = router
