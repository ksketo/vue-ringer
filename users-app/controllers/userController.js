const mongoose = require('mongoose')
const User = mongoose.model('User')
const redis = require('redis')

const client = redis.createClient(6379, 'redis')

exports.getUserDetails = async (req, res) => {
  // Query database for a list of all properties
  let users = await User.find().lean()
  return res.json(users)
}

exports.addUser = async (req, res) => {
  const user = await (new User(req.body)).save()
    .then(u => {
      client.publish('data', JSON.stringify(req.body))
      return res.status(200).send(u)
    })
    .catch(err => res.status(500).send('There was a problem adding the phone number.'))
}
