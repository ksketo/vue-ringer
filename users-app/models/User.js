const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const userSchema = new mongoose.Schema({
  phoneNumber: {
    type: String,
    trim: true
  },
  msgFrequency: {
    type: String,
    trim: true
  }
})

module.exports = mongoose.model('User', userSchema)
