const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

// Specify what to do with .js files
const javascript = {
  test: /\.(js)$/,
  use: [{
    loader: 'babel-loader',
    options: { presets: ['es2015'] }
  }]
}

// Compress our JS
const uglify = new webpack.optimize.UglifyJsPlugin({
  compress: { warnings: false }
})

// Put it all together
const config = {
  entry: {
    App: './public/javascripts/phone-app.js'
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'public', 'dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [javascript]
  },
  plugins: [uglify]
}
process.noDeprecation = true

module.exports = config
