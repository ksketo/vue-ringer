// import axios from 'axios'
import { $ } from './bling'

function ajaxPhone (e) {
  e.preventDefault()
  axios
    .post(this.action, {
      'phoneNumber': $('.number__input').value
    })
    // .post(this.action)
    .then(res => {
      $('.number__input').value = ''
    })
    .catch(err => {
      console.log('Error in axios POST request')
      console.error(err)
    })
}

export default ajaxPhone
