import { $, $$ } from './modules/bling'
import ajaxPhone from './modules/phone'

const phoneForms = $$('form.phone')
phoneForms.on('submit', ajaxPhone)
